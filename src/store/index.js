import { createStore } from 'vuex'

export default createStore({
  state: {
    todos: [
      {
        id: 1,
        title: "Canada"
      },
      {
        id: 2,
        title: "England"
      },
      {
        id: 3,
        title: "Norway"
      },
      {
        id: 4,
        title: "Germany"
      }
    ]
  },
  mutations: {
    ADD_TODO (state, payload) {
      state.todos.push(payload);
      console.log(payload);
    },
    DELETE_TODO (state, id) {
      state.todos = state.todos.filter((todo) => todo.id != id);
    },
    UPDATE_TODO (state, payload) {
      let index = state.todos.findIndex((todo) => todo.id == payload.id);
      if (index != -1) {
        state.todos[index] = payload;
        // console.log(payload);
      }
    }
  },
  actions: {
    addToDo ({commit}, payload) {
      commit("ADD_TODO", payload);
    },
    deleteToDo ({commit}, id) {
      commit("DELETE_TODO", id);
    },
    updateToDo ({commit}, payload) {
      commit("UPDATE_TODO", payload);
    }
  },
  modules: {
  },
  getters: {
    allTodos: (state) => state.todos
  }
})
